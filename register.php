<!DOCTYPE html>
<html lang="en">
<head>
    <?php  include('config.php'); ?>
    <?php  include('controller/user_controller.php'); ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/stylesheet.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>SSG News - Register</title>
</head>
<body>
<?php require_once "view/header.php"; ?>
<div>
    <form method="post" action="register.php" >
        <h2>Register</h2>
        <?php include('model/errors.php') ?>
        <ul class="list-group">
            <li class="list-group-item">
                <input  type="text" name="username" value="<?php echo $username; ?>"  placeholder="Username">
            </li>
            <li class="list-group-item">
                <input type="email" name="email" value="<?php echo $email ?>" placeholder="Email">
            </li>
            <li class="list-group-item">
                <input type="password" name="password_1" placeholder="Password">
            </li>
            <li class="list-group-item">
                <input type="password" name="password_2" placeholder="Password confirmation">
            </li>
            <li class="list-group-item">
                <button type="submit" class="btn btn-primary" name="reg_user">Register</button>
            </li>
            <li class="list-group-item">
                <p>
                    Already a member? <a href="login.php">Sign in</a>
                </p>
            </li>
        </ul>
    </form>
</div>

</body>
</html>
