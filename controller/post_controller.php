<?php
require_once "user_controller.php";
require_once "thumbnail_controller.php";
if (isset($_POST['create_post'])) { createPost($_POST); }

function getAllPosts() {
    global $conn;
    $sql = "SELECT * FROM post";
    $result = mysqli_query($conn, $sql);

    $posts = mysqli_fetch_all($result, MYSQLI_ASSOC);

    $final_posts = array();
    foreach ($posts as $post) {
        array_push($final_posts, $post);
    }
    return $final_posts;
}

function createPost($request_values)
{
    global $conn, $errors, $title, $featured_image, $license, $body;
    $title = esc($request_values['title']);
    $body = htmlentities(esc($request_values['body']));
    if (isset($request_values['license'])) {
        $license = esc($request_values['license']);
    }
    $user_id = $_SESSION["user"]["id"];

    if (empty($title)) {
        array_push($errors, "Post title is required");
    }
    if (empty($body)) {
        array_push($errors, "Post body is required");
    }
    if (empty($license)) {
        array_push($errors, "Post topic is required");
    }

    $featured_image = $_FILES['featured_image']['name'];
    if (empty($featured_image)) {
        array_push($errors, "Featured image is required");
    }

    $target = "resources/post_images/" . basename($featured_image);
    if ($_FILES["featured_image"]["size"] > 5000000000) {
        array_push($errors, "Image is too large");
    }
    if (!move_uploaded_file($_FILES['featured_image']['tmp_name'], $target)) {
        array_push($errors, "Failed to upload image. Please check file settings for your server");
    }

    //generateThumbnail($target);

    $post_check_query = "SELECT * FROM post WHERE title='$title' LIMIT 1";
    $result = mysqli_query($conn, $post_check_query);

    if (mysqli_num_rows($result) > 0) {
        array_push($errors, "A post already exists with that title.");
    }

    if (count($errors) == 0) {
        $query = "INSERT INTO post (user_id, title, img, text, license) VALUES('$user_id', '$title', '$featured_image', '$body', '$license')";
        if (mysqli_query($conn, $query)) {

            $_SESSION['message'] = "Post created successfully";
            header('location: index.php');
            exit(0);
        }
    }
}
?>