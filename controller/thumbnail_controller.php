<?php

function generateThumbnail($imagePath) {
    $imageData = getimagesize($imagePath);
    $width = $imageData[0];
    $height = $imageData[1];
    $imageType = $imageData[2];

    if ($width <= 128) {
        return $imagePath;
    }

    $image = null;
    if ($imageType == IMAGETYPE_PNG) {
        $image = imagecreatefrompng($imagePath);
    }
    else if ($imageType == IMAGETYPE_WEBP) {
        $image = imagecreatefromwebp($imagePath);
    }
    else if ($imageType == IMAGETYPE_JPEG) {
        $image = imagecreatefromjpeg($imagePath);
    }
    else if ($imageType == IMAGETYPE_GIF) {
        $image = imagecreatefromgif($imagePath);
    }
    else {
        return $imagePath;
    }

    $image = imagescale($image, 128);

    $pathInfo = pathinfo($imagePath);
    $thumbnailPath = $pathInfo["resources/thumbnails"] . DIRECTORY_SEPARATOR . $pathInfo["filename"] . " (Thumbnail)." . $pathInfo["extension"];

    if ($imageType == IMAGETYPE_PNG) {
        imagepng($image, $thumbnailPath);
    }
    else if ($imageType == IMAGETYPE_WEBP) {
        imagewebp($image, $thumbnailPath, 100);
    }
    else if ($imageType == IMAGETYPE_JPEG) {
        imagejpeg($image, $thumbnailPath, 100);
    }
    else if ($imageType == IMAGETYPE_GIF) {
        imagegif($image, $thumbnailPath);
    }

    return $thumbnailPath;
}

?>