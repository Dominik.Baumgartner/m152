<!DOCTYPE html>
<html lang="en">
<head>
    <?php  include('config.php'); ?>
    <?php  include('controller/user_controller.php'); ?>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/stylesheet.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>SSG News - Login</title>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
</head>
<body>

<?php require_once "view/header.php"; ?>

<h2>Login</h2>
<?php include('model/errors.php') ?>
<form action="login.php" method="post" >
        <div class="login_div">
            <ul class="list-group">
                <li class="list-group-item">
                    <input type="text" name="username" value="<?php echo $username; ?>" value="" placeholder="Username">
                </li>
                <li class="list-group-item">
                    <input type="password" name="password"  placeholder="Password">
                </li>
                <li class="list-group-item">
                    <button class="btn btn-primary" type="submit" name="login_btn">Login</button>
                </li>
                <li class="list-group-item">
                    <p>Don't have an account? <a href="register.php">Register</a></p>
                </li>

        </div>
    </form>





</body>
</html>