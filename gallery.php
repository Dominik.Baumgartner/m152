<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once('config.php') ?>
    <?php require_once('controller/post_controller.php') ?>
    <?php require_once('controller/user_controller.php') ?>
    <?php
    $posts = getAllPosts();
    $posts = array_reverse($posts);
    ?>

    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/stylesheet.css"/>
    <title>SSG News - Gallery</title>
</head>
<body>
<div>
    <?php require_once "view/header.php"; ?>
    <h2 class="content-title">Recent Articles</h2>
    <div class="content">
        <?php foreach ($posts as $post): ?>

            <div class="gallery">
                <picture class="post_image post_picture">
                    <img src="<?php echo 'resources/post_images/' . $post['img']; ?>" alt="Image not found"/>
                </picture>
            </div>

        <?php endforeach ?>
    </div>


</div>
</body>
</html>