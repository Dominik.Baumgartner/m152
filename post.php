<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once('config.php') ?>
    <?php require_once('controller/post_controller.php') ?>
    <?php require_once('controller/user_controller.php') ?>
    <?php $posts = getAllPosts(); ?>


    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/stylesheet.css"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>SSG News - Create Post</title>
</head>
<body>

<?php require_once "view/header.php"; ?>

<div>
    <div>
        <h2>Create Post</h2>
        <?php include('model/errors.php') ?>
        <form method="post" enctype="multipart/form-data" action="post.php" >
            <ul class="list-group">
                <li class="list-group-item">
                    <input type="text" name="title"placeholder="Title">
                </li>
                <li class="list-group-item">
                    <label>Featured image</label>
                </li>
                <li class="list-group-item">
                    <input type="file" accept=".png, .jpeg, .gif, .jpg" name="featured_image" >
                </li>
                <li class="list-group-item">
                    <label>Max file size: 5mb</label>
                </li>
                <li class="list-group-item">
                    <textarea name="body" id="body" cols="30" rows="10"></textarea>
                </li>
                <li class="list-group-item">
                    <select name="license">
                        <option value="All rights reserved" selected>All rights reserved</option>
                        <option value="CC BY-NC-ND">CC BY-NC-ND</option>
                        <option value="CC BY-ND">CC BY-ND</option>
                        <option value="CC BY-NC-SA">CC BY-NC-SA</option>
                        <option value="CC BY-NC">CC BY-NC</option>
                        <option value="CC BY-SA">CC BY-SA</option>
                        <option value="CC BY">CC BY</option>
                        <option value="CC0 / Public Domain">CC0 / Public Domain</option>
                    </select>
                </li>
                <li class="list-group-item">
                    <button type="submit" class="btn btn-primary" name="create_post">Save Post</button>
                </li>
            </ul>
        </form>
    </div>

</div>
</body>
</html>

<script>
    CKEDITOR.replace('body');
</script>