<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once('config.php') ?>
    <?php require_once('controller/post_controller.php') ?>
    <?php require_once('controller/user_controller.php') ?>
    <?php
    $posts = getAllPosts();
    $posts = array_reverse($posts);
    ?>

    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="css/stylesheet.css"/>
    <title>SSG News - Home</title>
</head>
<body>
<div>
    <?php require_once "view/header.php"; ?>
    <h2 class="content-title">Recent Articles</h2>
    <div class="content">
        <?php foreach ($posts as $post): ?>

        <div class="post flex-container">
            <picture class="post_image post_picture">
                <img src="<?php echo 'resources/post_images/' . $post['img']; ?>" class="post_image" alt="Image not found"/>
                <p class="license">
                    <?php
                    echo $post['license']
                    ?>
                </p>
            </picture>
            <div class="post_info">
                <h3><?php echo $post['title'] ?></h3>
                <p><?php
                     $user = getUserById($post['user_id']);
                    echo $user['username'];
                    ?></p>
                <p>
                    <?php echo $post['text'] ?>
                </p>
            </div>
        </div>

        <?php endforeach ?>
    </div>


</div>
</body>
</html>